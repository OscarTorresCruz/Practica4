<?php
	$un_bool = TRUE;                       //Valor Booldeano
	$un_str = "Programacion";             //Una Cadena
	$un_str2 = 'Programacion';           //Una Cadena
	$un_int = 12;                       //Un Entero
	
	echo gettype($un_bool);       //Imprime: Booleano
	echo gettype($un_str);        //Imprime: String
	
	//Si este valor es un entero, incrementarlo en cuatro
	
	if(is_int($un_int)){
		$un_int +4;
	}
	
	// Si $bool es una cadena, imprimirla
	// (no imprime nada)
	
	if (is_string($un_bool)){
	echo "cadena: $un_bool";
	}
?>	