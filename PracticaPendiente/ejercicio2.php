<html>
	<head>
		<title> Uso de Método POST </title>
	</head>
	<body>
		<h2> Conversión Décimal a Binario </h2>
		
		<form name = "frmPost" method = "POST">
			Escriba el Décimal:
			<input type = "text" name = "txtNumero">
			<input type = "submit" name = "btnEnviar">
		</form>

		<?php 

			if (!empty($_GET['txtNumero'])){
				$numDecimal = $_GET['txtNumero'];
				echo "El numero decimal es: ". $numDecimal. "<br/>";
				
				$numBinario = ' ';
				do{
					$numBinario = $numDecimal % 2 . $numBinario;
					$numDecimal = (int) ($numDecimal/2);
				}while ($numDecimal > 0);
					echo "Numero Binario: " . $numBinario;
				
			}
		?>
	</body>
</html>	